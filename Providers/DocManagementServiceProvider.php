<?php

namespace Modules\DocManagement\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class DocManagementServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerDisks();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('docmanagement.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php',
            'docmanagement'
        );
    }

    /**
     * Register Disk
     * @return void
     */
    protected function registerDisks()
    {
        $disks = array_merge(app()->config['filesystems.disks'], config('docmanagement.filesystems.disks'));
        app()->config['filesystems.disks'] = $disks;
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/docmanagement');

        $sourcePath = __DIR__.'/../Resources/views';

        $customViewsFolder = $sourcePath . '/backpack/crud';//resource_path('views/vendor/backpack/crud');

        // LOAD THE VIEWS For Backpack!!
        // - first the published/overwritten views (in case they have any changes)
        if (file_exists($customViewsFolder)) {
            $this->loadViewsFrom($customViewsFolder, 'crud');
        }

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

//        $this->publishes([
//            __DIR__.'/Resources/views/backpack/crud' => resource_path('views/vendor/backpack/crud')
//        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/docmanagement';
        }, \Config::get('view.paths')), [$sourcePath]), 'docmanagement');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/docmanagement');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'docmanagement');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'docmanagement');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
