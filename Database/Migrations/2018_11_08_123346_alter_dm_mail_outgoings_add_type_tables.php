<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDmMailOutgoingsAddTypeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('dm_mail_outgoings', function (Blueprint $table) {
            $table->string('type')->after('sender');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dm_mail_outgoings', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
