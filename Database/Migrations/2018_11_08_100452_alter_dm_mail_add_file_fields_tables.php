<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDmMailAddFileFieldsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dm_mail_incomings', function (Blueprint $table) {
            $table->string('files')->after('sender')->nullable();
            $table->string('upload')->after('files')->nullable();
        });
        Schema::table('dm_mail_outgoings', function (Blueprint $table) {
            $table->string('files')->after('sender')->nullable();
            $table->string('upload')->after('files')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dm_mail_incomings', function (Blueprint $table) {
            $table->dropColumn('files');
            $table->dropColumn('upload');
        });
        Schema::table('dm_mail_outgoings', function (Blueprint $table) {
            $table->dropColumn('files');
            $table->dropColumn('upload');
        });
    }
}
