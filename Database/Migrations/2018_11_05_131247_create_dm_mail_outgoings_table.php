<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDmMailOutgoingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dm_mail_outgoings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('received_by')->nullable();
            $table->string('to');
            $table->string('subject');
            $table->string('sender');

            $table->index('sender');
            $table->index('to');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dm_mail_outgoings');
    }
}
