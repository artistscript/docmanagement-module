<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDmMailTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dm_mail_incomings', function (Blueprint $table) {
            $table->string('number')->after('sender')->nullable();
        });
        Schema::table('dm_mail_outgoings', function (Blueprint $table) {
            $table->string('number')->after('sender')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dm_mail_incomings', function (Blueprint $table) {
            $table->dropColumn('number');
        });
        Schema::table('dm_mail_outgoings', function (Blueprint $table) {
            $table->dropColumn('number');
        });
    }
}
