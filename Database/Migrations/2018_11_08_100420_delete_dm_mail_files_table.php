<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteDmMailFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('dm_mail_files');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('dm_mail_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fileable_id')->unsigned();
            $table->string('fileable_type');
            $table->text('file');
            $table->timestamps();
        });
    }
}
