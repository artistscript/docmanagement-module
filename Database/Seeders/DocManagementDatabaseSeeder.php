<?php

namespace Modules\DocManagement\Database\Seeders;

use App\Models\MenuItem;
use App\Models\Permission;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DocManagementDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $menu = MenuItem::create([
            "name" => "Document Managements",
            "type" => "internal_link",
            "link" => "#",
            "icon" => "fa-book",
            "page_id" => null,
            "parent_id" => 0,
            "lft" => null,
            "rgt" => null,
            "depth" => null,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "deleted_at" => null
        ]);

        $mails = MenuItem::create([
            "name" => "Mails",
            "type" => "internal_link",
            "link" => "#",
            "icon" => "fa-mail-bulk",
            "page_id" => null,
            "parent_id" => $menu->id,
            "lft" => null,
            "rgt" => null,
            "depth" => null,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "deleted_at" => null
        ]);

        $incoming = MenuItem::create([
            "name" => "Incoming",
            "type" => "internal_link",
            "link" => "documents/mails/incoming",
            "icon" => "",
            "page_id" => null,
            "parent_id" => $mails->id,
            "lft" => null,
            "rgt" => null,
            "depth" => null,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "deleted_at" => null
        ]);

        $outgoing = MenuItem::create([
            "name" => "Outgoing",
            "type" => "internal_link",
            "link" => "documents/mails/outgoing",
            "icon" => "",
            "page_id" => null,
            "parent_id" => $mails->id,
            "lft" => null,
            "rgt" => null,
            "depth" => null,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "deleted_at" => null
        ]);

        $role = Role::find(1);
        $role->menuitems()->attach($menu->id);
        $role->menuitems()->attach($mails->id);
        $role->menuitems()->attach($incoming->id);
        $role->menuitems()->attach($outgoing->id);

        $permission = Permission::create([
            'name' => 'IncomingController@index',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'IncomingController@create',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'IncomingController@store',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'IncomingController@show',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'IncomingController@edit',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'IncomingController@update',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'OutgoingController@index',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'OutgoingController@create',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'OutgoingController@store',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'OutgoingController@show',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'OutgoingController@edit',
        ]);
        $role->givePermissionTo($permission);
        $permission = Permission::create([
            'name' => 'OutgoingController@update',
        ]);
        $role->givePermissionTo($permission);

        // seed settings table
        $this->call(SettingsTableSeeder::class);

        cache()->tags('menuTree')->flush();
    }
}
