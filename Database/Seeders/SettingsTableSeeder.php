<?php

namespace Modules\DocManagement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getSettings() as $index => $setting) {
            $result = \DB::table('settings')->insert($setting);

            if (!$result) {
                $this->command->info("Insert failed at record $index.");

                return;
            }
        }
    }

    public function getSettings()
    {
        return [
            [
                'key'         => 'dm_outgoing_types',
                'name'        => 'Ougoing Mail Types',
                'description' => 'Type of Outgoing Mails.',
                'value'       => '[{"name":"MoU"},{"name":"Other"}]',
                'field'       => '{"name":"value","label":"Value","type":"table","entity_singular":"type","columns":{"name":"Name"}}',
                'active'      => 1,
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
            ],
        ];
    }
}
