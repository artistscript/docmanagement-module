<?php

namespace Modules\DocManagement\Models\Mail;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class File extends Model
{
    use CrudTrait, LogsActivity;
    protected $table = 'dm_mail_files';
    protected $fillable = ['fileable_id', 'fileable_type', 'file'];
    protected $logAttributes = ['fileable_id', 'fileable_type', 'file'];

    public function fileable()
    {
        return $this->morphTo();
    }
}
