<?php

namespace Modules\DocManagement\Models\Mail;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use Modules\DocManagement\Traits\DocumentUpload;

class Incoming extends Model
{
    use CrudTrait, LogsActivity, DocumentUpload;
    protected $table = 'dm_mail_incomings';

    protected $fillable = ['date', 'received_by', 'to', 'subject', 'sender', 'number', 'notes', 'files', 'upload'];

    protected static $logAttributes = ['date', 'received_by', 'to', 'subject', 'sender', 'number', 'notes', 'files', 'upload'];

    protected $casts = [
        'files' => 'array',
        'date' => 'date'
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($obj) {
            // deleting upload file
            if(isset($obj->upload) && !empty($obj->upload)){
                \Storage::disk('dm_mails')->delete($obj->upload);
            }

            // deleting upload images
            if (count((array)$obj->files)) {
                foreach ($obj->files as $file_path) {
                    \Storage::disk('dm_mails')->delete($file_path);
                }
            }
        });
    }
}
