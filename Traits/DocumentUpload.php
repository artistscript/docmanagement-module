<?php

namespace Modules\DocManagement\Traits;

use Backpack\CRUD\CrudTrait;

trait DocumentUpload
{
    /**
     * @param $value
     */
    public function setFilesAttribute($value)
    {
        $attribute_name = 'files';
        $disk = 'dm_mails';
        $destination_path = explode('_', $this->table)[2];
        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }

    /**
     * @param $value
     */
    public function setUploadAttribute($value)
    {
        $attribute_name = 'upload';
        $disk = 'dm_mails';
        $destination_path = explode('_', $this->table)[2];
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
}
