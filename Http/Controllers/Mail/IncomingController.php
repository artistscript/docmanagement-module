<?php

namespace Modules\DocManagement\Http\Controllers\Mail;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Modules\DocManagement\Http\Requests\Mails\IncomingStoreRequest;
use Modules\DocManagement\Http\Requests\Mails\IncomingUpdateRequest;

class IncomingController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('Modules\DocManagement\Models\Mail\Incoming');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/documents/mails/incoming');
        $this->crud->setEntityNameStrings('Incoming Mail', 'Incoming Mails');

        // removing all action buttons
        $this->crud->removeAllButtonsFromStack('line');

        // allow show item
        $this->crud->allowAccess(['show']);

        // set show view to use
        $this->crud->setShowView('docmanagement::view_mail');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumns([
            [
                'name' => 'date',
                'type' => 'date',
                'label' => 'Date',
                'format' => 'j F Y',
            ],
            [
                'name' => 'number',
                'type' => 'doc_number',
                'label' => 'Number',
            ],
            [
                'name' => 'to',
                'type' => 'text',
                'label' => 'To',
            ],
            [
                'name' => 'sender',
                'type' => 'text',
                'label' => 'From / Sender',
            ],
            [
                'name' => 'subject',
                'type' => 'text',
                'label' => 'Subject',
            ],
        ]);

        /*
         |--------------------------------------------------------------------------
         | CrudField Configuration
         |--------------------------------------------------------------------------
         */
        $this->crud->addFields([
            [
                'name' => 'date',
                'type' => 'date_picker',
                'label' => 'Date',
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                ],

            ],
            [
                'name' => 'number',
                'type' => 'text',
                'label' => 'Number',
            ],
            [
                'name' => 'to',
                'type' => 'text',
                'label' => 'To',
            ],
            [
                'name' => 'sender',
                'type' => 'text',
                'label' => 'From / Sender',
            ],
            [
                'name' => 'subject',
                'type' => 'text',
                'label' => 'Subject',
            ],
            [
                'name' => 'received_by',
                'type' => 'text',
                'label' => 'Received By',
            ],
            [
                'name' => 'notes',
                'type' => 'textarea',
                'label' => 'Notes'
            ],
            [
                'name' => 'files',
                'label' => 'Files',
                'type' => 'upload_multiple_file_limit',
                'upload' => true,
                'disk' => 'dm_mails',
                'store_in' => 'files',
                'limit' => [
                    'image/jpeg',
                    'image/jpg',
                    'image/png',
                ]
            ],
            [
                'name' => 'upload',
                'label' => 'Attachment',
                'type' => 'upload_file_limit',
                'upload' => true,
                'disk' => 'dm_mails',
                'limit' => [
                    'application/zip'
                ]
            ],
        ]);

        /*
         |--------------------------------------------------------------------------
         | Filter Section
         |--------------------------------------------------------------------------
         */


        // add asterisk for fields that are required
        $this->crud->setRequiredFields(IncomingStoreRequest::class, 'create');
        $this->crud->setRequiredFields(IncomingUpdateRequest::class, 'edit');
    }

    public function store(IncomingStoreRequest $request)
    {
        return parent::storeCrud($request);
    }

    public function update(IncomingUpdateRequest $request)
    {
        return parent::updateCrud($request);
    }
}
