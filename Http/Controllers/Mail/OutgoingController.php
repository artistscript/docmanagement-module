<?php

namespace Modules\DocManagement\Http\Controllers\Mail;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Modules\DocManagement\Http\Requests\Mails\OutgoingStoreRequest;
use Modules\DocManagement\Http\Requests\Mails\OutgoingUpdateRequest;

class OutgoingController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('Modules\DocManagement\Models\Mail\Outgoing');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/documents/mails/outgoing');
        $this->crud->setEntityNameStrings('Outgoing Mail', 'Outgoing Mails');

        // removing all action buttons
        $this->crud->removeAllButtonsFromStack('line');

        // allow show item
        $this->crud->allowAccess(['show']);

        // set show view to use
        $this->crud->setShowView('docmanagement::view_mail');

        $mailTypes = collect(json_decode(\Config::get('settings.dm_outgoing_types')))->pluck('name','name');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumns([
            [
                'name' => 'date',
                'type' => 'date',
                'label' => 'Date',
            ],
            [
                'name' => 'number',
                'type' => 'doc_number',
                'label' => 'Number',
            ],
            [
                'name' => 'to',
                'type' => 'text',
                'label' => 'To',
            ],
            [
                'name' => 'sender',
                'type' => 'text',
                'label' => 'From / Sender',
            ],
        ]);

        /*
         |--------------------------------------------------------------------------
         | CrudField Configuration
         |--------------------------------------------------------------------------
         */
        $this->crud->addFields([
            [
                'name' => 'date',
                'type' => 'date_picker',
                'label' => 'Date',
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                ],

            ],
            [
                'name' => 'number',
                'type' => 'text',
                'label' => 'Number',
            ],
            [
                'name' => 'to',
                'type' => 'text',
                'label' => 'To',
            ],
            [
                'name' => 'sender',
                'type' => 'text',
                'label' => 'From / Sender',
            ],
            [
                'name' => 'type',
                'label' => "Mail Type",
                'type' => 'select2_from_array',
                'options' => $mailTypes,
                'allows_null' => false,
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name' => 'subject',
                'type' => 'text',
                'label' => 'Subject',
            ],
            [
                'name' => 'received_by',
                'type' => 'text',
                'label' => 'Received By',
            ],
            [
                'name' => 'notes',
                'type' => 'textarea',
                'label' => 'Notes'
            ],
            [
                'name' => 'files',
                'label' => 'Files',
                'type' => 'upload_multiple_file_limit',
                'upload' => true,
                'disk' => 'dm_mails',
                'store_in' => 'files',
                'limit' => [
                    'image/jpeg',
                    'image/jpg',
                    'image/png',
                ]
            ],
            [
                'name' => 'upload',
                'label' => 'Attachment',
                'type' => 'upload_file_limit',
                'upload' => true,
                'disk' => 'dm_mails',
                'limit' => [
                    'application/zip'
                ]
            ],
        ]);

        // add asterisk for fields that are required
        $this->crud->setRequiredFields(OutgoingStoreRequest::class, 'create');
        $this->crud->setRequiredFields(OutgoingUpdateRequest::class, 'edit');
    }

    public function store(OutgoingStoreRequest $request)
    {
        return parent::storeCrud($request);
    }

    public function update(OutgoingUpdateRequest $request)
    {
        return parent::updateCrud($request);
    }
}
