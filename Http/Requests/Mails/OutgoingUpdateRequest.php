<?php

namespace Modules\DocManagement\Http\Requests\Mails;

use Illuminate\Foundation\Http\FormRequest;

class OutgoingUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'number' => 'required',
            'to' => 'required',
            'subject' => 'required',
            'sender' => 'required',
            'type' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
