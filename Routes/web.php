<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin') . '/documents/mails',
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => '\Modules\DocManagement\Http\Controllers\Mail',
], function () {
    CRUD::resource('incoming', 'IncomingController');
    CRUD::resource('outgoing', 'OutgoingController');
});
