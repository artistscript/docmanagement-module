@extends('backpack::layout')
@section('header')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('backpack::crud.preview') }}</li>
        </ol>
    </section>
@endsection
@section('content')
    @if ($crud->hasAccess('list'))
        <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                {{ trans('backpack::crud.preview') }}
                <span class="text">{{ ucfirst($crud->entity_name) }}</span>
            </h3>
        </div>
        <div class="box-body">
            @php
                $outlet = $crud->entry;
                //dump($outlet->district);
            @endphp
            <div class="row">
                <div class="col-md-3">
                    <div class="box box-solid box-success">
                        <div class="box-header">
                             <h3 class="box-title"><i class="fa fa-barcode"></i>&nbsp;Mail Number:</h3>
                        </div>
                        <div class="box-body">
                            <strong>{{ $outlet->number }}</strong>
                        </div>
                    </div>
                    <div class="box box-solid box-success">
                        <div class="box-header">
                             <h3 class="box-title"><i class="fa fa-calendar"></i>&nbsp;Incoming Date:</h3>
                        </div>
                        <div class="box-body">
                            <strong>{{ $outlet->date->format('j F Y') }}</strong>
                        </div>
                    </div>
                    <div class="box box-solid box-success">
                        <div class="box-header">
                             <h3 class="box-title"><i class="fa fa-envelope"></i>&nbsp;From / Sender:</h3>
                        </div>
                        <div class="box-body">
                            <strong>{{ $outlet->sender }}</strong>
                        </div>
                    </div>
                    <div class="box box-solid box-success">
                        <div class="box-header">
                             <h3 class="box-title"><i class="fa fa-comments"></i>&nbsp;Subject:</h3>
                        </div>
                        <div class="box-body">
                            <strong>{{ $outlet->subject }}</strong>
                        </div>
                    </div>
                    <div class="box box-solid box-success">
                        <div class="box-header">
                             <h3 class="box-title"><i class="fa fa-user"></i>&nbsp;To:</h3>
                        </div>
                        <div class="box-body">
                            <strong>{{ $outlet->to }}</strong>
                        </div>
                    </div>
                    <div class="box box-solid box-success">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-sticky-note"></i>&nbsp;Notes:</h3>
                        </div>
                        <div class="box-body">
                            <strong>{{ $outlet->notes }}</strong>
                        </div>
                    </div>
                    <div class="box box-solid box-success">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fas fa-paperclip"></i>&nbsp;Attachment:</h3>
                        </div>
                        <div class="box-body">
                            @if(isset($outlet->upload) && !empty($outlet->upload))
                            <strong>
                                <a href="/storage/{{ $outlet->upload }}">
                                    Download
                                </a>
                            </strong>
                            @else
                                <strong>No Attached File Available</strong>
                            @endif
                        </div>
                    </div>
                    <div>
                        <a class="btn btn-lg btn-block btn-warning" href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}" role="button">
                            <i class="fa fa-edit"></i> {{ trans('backpack::crud.edit') }}
                        </a>
                        <a class="btn btn-danger btn-lg btn-block" href="javascript:void(0)" onclick="deleteEntry(this)" data-route="{{ url($crud->route.'/'.$entry->getKey()) }}" data-button-type="delete">
                            <i class="fa fa-trash"></i> {{ trans('backpack::crud.delete') }}
                        </a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="box box-solid box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-briefcase"></i>&nbsp;File </h3>
                        </div>
                        @if(!empty($outlet->files))
                            @foreach ($outlet->files as $file)
                                <div class="box-body"><img class="img-responsive" src="/storage/{{ $file }}" width="760px" height="1200px">
                            @endforeach
                        @endif
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
        </div>
    </div>
@endsection

@push('after_scripts')
    <script>
        if (typeof deleteEntry != 'function') {
            $("[data-button-type=delete]").unbind('click');

            function deleteEntry(button) {
                // ask for confirmation before deleting an item
                // e.preventDefault();
                var button = $(button);
                var route = button.attr('data-route');
                var row = $("#crudTable a[data-route='"+route+"']").closest('tr');

                if (confirm("{{ trans('backpack::crud.delete_confirm') }}") == true) {
                    $.ajax({
                        url: route,
                        type: 'DELETE',
                        success: function(result) {
                            // Show an alert with the result
                            new PNotify({
                                title: "{{ trans('backpack::crud.delete_confirmation_title') }}",
                                text: "{{ trans('backpack::crud.delete_confirmation_message') }}",
                                type: "success"
                            });

                            // Hide the modal, if any
                            $('.modal').modal('hide');

                            // Redirect back the user
                            window.location.href = "{{ url($crud->route) }}";
                        },
                        error: function(result) {
                            // Show an alert with the result
                            new PNotify({
                                title: "{{ trans('backpack::crud.delete_confirmation_not_title') }}",
                                text: "{{ trans('backpack::crud.delete_confirmation_not_message') }}",
                                type: "warning"
                            });
                        }
                    });
                } else {
                    // Show an alert telling the user we don't know what went wrong
                    new PNotify({
                        title: "{{ trans('backpack::crud.delete_confirmation_not_deleted_title') }}",
                        text: "{{ trans('backpack::crud.delete_confirmation_not_deleted_message') }}",
                        type: "info"
                    });
                }
            }
        }

        // make it so that the function above is run after each DataTable draw event
        // crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');
    </script>
@endpush
