<?php

namespace Modules\DocManagement;

use App\Models\Permission;
use App\Models\Role;
use App\Repositories\MenuItems;
use Modules\Manager\Manager;

class DocManagement extends Manager
{
    public function __construct()
    {
        parent::__construct();
        $this->module('DocManagement');
        return $this;
    }

    public function removeAdditionalPermission()
    {
        $role = Role::find(1);

        // find permissions
        $permissions = Permission::where('name', 'like', "Incoming%")->get();
        // detach permissions from role
        $role->permissions()->detach($permissions);// delete permissions
        foreach ($permissions as $permission) {
            $permission->delete();
        }

        // find permissions
        $permissions = Permission::where('name', 'like', "Outgoing%")->get();
        // detach permissions from role
        $role->permissions()->detach($permissions);// delete permissions
        foreach ($permissions as $permission) {
            $permission->delete();
        }

        $this->removeAdditionalMenu($role);
        $this->removeAdditionalSettings();
    }

    public function removeAdditionalMenu($role)
    {
        $this->sendInfo("Removing Menus for Document Managements");
        $menu = new MenuItems;
        $menus = [
            $menu->findByName("Document Managements"),
            $menu->findByName("Mails"),
            $menu->findByName("Incoming"),
            $menu->findByName("Outgoing"),
        ];

        foreach ($menus as $key => $value) {
            $role->menuitems()->detach($value);
            $value->delete();
        }
    }

    public function removeAdditionalSettings()
    {
        if (\DB::table('settings')->where('key','like','dm_%')->count() > 0) {
            \DB::table('settings')->where('key','like','dm_%')->delete();
        }
    }
}
