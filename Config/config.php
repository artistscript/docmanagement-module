<?php

return [
    'name' => 'DocManagement',

    /**
     * Filesystem to be pushed to main system
     */
    'filesystems' => [
        'disks' => [
            'dm_mails' => [
                'driver' => 'local',
                'root' => storage_path(
                    'document_managements'
                    . DIRECTORY_SEPARATOR
                    . 'mails'
                ),
            ],
            'dm_templates' => [
                'driver' => 'local',
                'root' => storage_path(
                    'document_managements'
                    . DIRECTORY_SEPARATOR
                    . 'templates'
                ),
            ],
        ],
    ],
];
